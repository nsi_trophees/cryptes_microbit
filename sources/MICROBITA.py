"""


Auteur: Clovis Ricouart

Interface: microbit

Nom du projet: MICROBITA

Description: No-description

Toolbox: vittascience

Mode: code

Blocks: <xml xmlns="https://developers.google.com/blockly/xml"><block type="on_start" id="G[=T#8yqB70`NFgYq}GP" deletable="false" x="0" y="0"></block><block type="forever" id="o[WN]+eeF.OUxGch67@8" x="200" y="0"></block></xml>

Projet généré par Vittascience.

Ce fichier contient le code textuel ainsi que le code blocs. Il peut être importé de nouveau

sur l'interface http://vittascience.com/microbit


"""

#import des librairies
from microbit import *
import radio
import utime

#configuration de la radio
radio.on()
radio.config(channel = 7, power = 6, length = 32, group = 0)

#Variables
dico_id = {1:0,2:0}
cle_chiffrement = 7
cle_dechiffrement = -7

#definiton des fonctions

def crypt(txt, cle):
  message = ""
  for char in txt:
    value = ord(char) + cle
    message += chr(value % 128)
  return message


def envoi(ide):
    msg_env = str(ide) + '_' + str(dico_id[ide]) + '_' + txt
    msg_env = crypt(msg_env, cle_chiffrement)
    radio.send(msg_env)
    sleep(200)
    dico_id[ide] += 1
    sleep(100)
    display.show(Image.YES)
    sleep(200)
    display.clear()

while True:
  #envoi des messages aux clients
  if button_a.is_pressed():
    temp = temperature()
    txt = str(temp)
    envoi(1)


  if button_b.is_pressed():
    lum = display.read_light_level()
    txt = str(lum)
    envoi(2)

  stringdata = radio.receive()
  if stringdata:
    mess = crypt(stringdata, cle_dechiffrement).split('_')
    cle = mess[0]
    if int(cle) in dico_id:
      if int(mess[1]) == dico_id[int(cle)]:#à séparé
        display.scroll(mess[2])
        dico_id[int(cle)] +=1
