"""


Auteur: Clovis Ricouart

Interface: microbit

Nom du projet: MicroBITC

Description: No-description

Toolbox: vittascience

Mode: code

Blocks: <xml xmlns="https://developers.google.com/blockly/xml"><block type="on_start" id="G[=T#8yqB70`NFgYq}GP" deletable="false" x="0" y="0"></block><block type="forever" id="o[WN]+eeF.OUxGch67@8" x="200" y="0"></block></xml>

Projet généré par Vittascience.

Ce fichier contient le code textuel ainsi que le code blocs. Il peut être importé de nouveau

sur l'interface http://vittascience.com/microbit


"""



from microbit import *
import radio
import random

#configuration radio
radio.on()
radio.config(channel = 7, power = 6, length = 32, group = 0)


#variables
cle = "1_0"
cle_chiffrement = 7
cle_dechiffrement = -7

#fonction
def crypt(txt, cle):
  message = ""
  for char in txt:
      value = ord(char) + cle
      message += chr(value % 128)
  return message


while True:
  #Envoi de message
  if button_a.is_pressed():
    msg_env = cle + "_temperature"
    msg_env = crypt(msg_env, cle_chiffrement)
    radio.send(msg_env)
    cle = cle.split('_')
    cle = cle[0] + '_' + str(int(cle[1]) + 1)
    sleep(100)
    display.show(Image.YES)
    sleep(200)
    display.clear()
        
  #Reception de message
  stringdata = radio.receive()

  if stringdata:
    
    mess = crypt(stringdata, cle_dechiffrement).split('_')
    
    cle_serv = mess[0]+ '_' + mess[1] 
    texte = mess[2]
    if cle_serv == cle :
      display.scroll(texte)
      cle = cle.split('_')
      cle = cle[0] + '_' + str(int(cle[1]) + 1)   